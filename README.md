# Nextcloud TK Style
My personal tweaks of the nextcloud style in order to make it look cleaner, adding transitions, removing some borders and adding more discrete shadows. 

![Screenshot](screenshot.png)

This aggregation of tweaks was created to work on nextcloud version 14 and may not work on future versions or on some part of nextcloud.
## How to use it
To use it, you will have to activate the "custom CSS" app, then just paste the content of style.css into the custom CSS field in your settings. 
If you do not use the nextcloud original color you will have to change the main color variables at the top of the file to match with the colors you use on your nextcloud instance. 
(Personally, I use #400CD0)

